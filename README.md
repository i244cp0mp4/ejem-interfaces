## Requisitos técnicos
-  Java 8 o superior.
-  Apache maven versión 3.6.0 o más reciente (recomendada v3.6.2).
-  Un editor de textos minimalista (vi/vim, emacs o nano).

## Para empezar a trabajar
1.  Debes hacer [fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
 del proyecto y copiarlo a tus proyectos de GitLab.
2.  Deberás crear una rama de master y llamarla `<primer_apellido><primer_nombre>-interfaces`
3.  Cuando hayas concluído tus tareas, deberás enviar en un correo electrónico
    la ubicación del repositorio. Asegúrándote que se haya agregado al ayudante de laboratorio
    como _developer_ en tu repositorio final.

## Para probar correcto funcionamiento al hacer fork
-  Correr pruebas:
```bash
$ mvn clean
$ mvn test
```    
Después de ejecutar esto y si todo está bien, debería aparecerte un mensaje
como el siguiente:
```bash
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 5, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.618 s
```
## Puntos a desarrollar
Se proporcionan dos archivos: 
  -  Una interfaz (`Vehículo.java `) y 
  -  Una clase (`Auto.java`).
  
En el archivo `src/test/java/unam/fciencias/icc/AutoTest.java` están indicadas
las pruebas que tendrás que modificar (**MODIFICAR**) o implementar (**ESCRIBIR**);
cada una de estas pruebas tiene un comentario describiendo qué es lo que se espera
probar en cada una de ellas, de tal forma que sean una guía para realizar 
adecuaciones al código presentado en la clase `src/main/java/unam/fciencias/icc/Auto.java`.

Existen dos pruebas de ejemplo las cuales puedes apoyarte para escribir o modificar
aquellas que lo requieran.

Puedes usar cualquier método de la clase _Assert_ de [JUnit](https://junit.org/junit4/javadoc/latest/index.html)
 para facilitar las pruebas. Es importante mencionar que habrá casos en lo que además
 de escribir la prueba tendrás que implementar/modificar el código de la clase.

Trata de desarrollar primero las pruebas y después escribe el código que haga pasar
esas pruebas.

Como recomendación puedes hacer un commit por cada prueba que desarrolles y pase.

Cualquier duda puedes enviarla al grupo de correo donde se compartió este repositorio.
 

